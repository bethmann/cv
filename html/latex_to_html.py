# Description: Converts a LaTeX CV to an HTML CV.
# Author: Arne Bethmann
# Structure: The script reads a LaTeX file, extracts the personal information and sections, and converts them to HTML.
# Usage: python latex_to_html.py <filename>
# Output: index.html
# Functions: clean_latex_content, find_matching_brace, parse_cventry, parse_latex_environment, clean_field, extract_personal_info, parse_bibtex_file, generate_header_html, generate_bio_html, generate_publications_html, process_section, parse_section, main
# Constants: HEADER_TEMPLATE, BIO_TEMPLATE, SECTION_TEMPLATE, ENTRY_TEMPLATE, HTML_TEMPLATE, SECTIONS, BIO, LATEX_CHARS
# Dependencies: bibtexparser

import re
import sys
import bibtexparser

# Read HTML Templates from files
def read_template(file_path):
    with open(file_path, 'r') as file:
        return file.read()

HEADER_TEMPLATE = read_template('templates/header_template.html')
BIO_TEMPLATE = read_template('templates/bio_template.html')
SECTION_TEMPLATE = read_template('templates/section_template.html')
ENTRY_TEMPLATE = read_template('templates/entry_template.html')
HTML_TEMPLATE = read_template('templates/html_template.html')

# LaTeX Section Definitions
SECTIONS = {
    "Career": ["Year", "Position", "Institution", "Location", "Details"],
    "Education": ["Year", "Degree", "Institution", "Location", "Details"],
    "Funding": ["Year", "Project", "Details", "Duration", "Funder", "Amount"],
    "Research projects": ["Year", "Project", "Collaborators", "Institution", "Link"],
    "Publications": ["Year", "Title", "Journal", "Details"],
    "Presentations": ["Year", "Title", "Event", "Location", "Link"],
    "Teaching": ["Year", "Course", "Details", "Institution", "Link"],
    "Research visits": ["Year", "Institution", "Department", "Location", "Details"],
    "Consulting": ["Year", "Project", "Details", "Location", "Institution"],
    "Commissions": ["Year", "Role", "Details", "Institution", "Location"],
    "Reviewer": ["Journal", "Publisher", "ISSN", "Details"]
}

# Biography
BIO = "<strong>tl;dr</strong>: I am a survey methodologist and data scientist with a PhD in Social Sciences, currently serving as the Country Team Leader and a Principal Investigator for the German sub-study of the Survey of Health, Ageing and Retirement in Europe (<a href=\"https://www.share-eric.eu\" target=\"_blank\" rel=\"noopener noreferrer\">SHARE</a>), hosted at the SHARE Berlin Institute. Specializing in data quality improvement through innovative methods such as <a href=\"https://doi.org/10.48550/arXiv.2312.16887\" target=\"_blank\" rel=\"noopener noreferrer\">machine learning</a> and <a href=\"https://doi.org/10.1007/s11135-021-01156-0\" target=\"_blank\" rel=\"noopener noreferrer\">qualitative interviewing</a>, I have developed research infrastructures and taught survey methodology internationally at institutions like the Institute for Employment Research (IAB), the German Youth Institute (DJI), and the Max Planck Institute for Social Law and Social Policy (MPISOC). My research focuses on social inequality, poverty, family dynamics, and health outcomes, aiming to strengthen the empirical foundation for socially relevant research. Outside of work, I enjoy <a href=\"https://www.igsm.info/\" target=\"_blank\" rel=\"noopener noreferrer\">river surfing in Munich</a>."

# LaTeX Special Characters
LATEX_CHARS = {
    '\\&': '&amp;',
    '\\%': '%',
    '\\$': '$',
    '\\_': '_',
    '\\#': '#',
    '\\{': '{',
    '\\}': '}',
    '~': '&nbsp;',
    '\\texteuro': '&euro;',
    '--': '&ndash;',
    '---': '&ndash;',
    '\\normalfont': '',
    '\\newline': '',
}

# Utility Functions
def clean_latex_content(content, latex_chars):
    """Enhanced LaTeX content cleaning"""
    content = re.sub(r'%.*', '', content)
    content = re.sub(r'\\textbf\{(.*?)\}', r'<strong>\1</strong>', content)
    content = re.sub(r'\\emph\{(.*?)\}', r'<em>\1</em>', content)
    content = re.sub(r'\\href\{(.*?)\}\{(.*?)\}', r'<a href="\1" target="_blank" rel="noopener noreferrer">\2</a>', content)
    for latex, html in latex_chars.items():
        content = content.replace(latex, html)
    return content

def find_matching_brace(text, start=0):
    """Finds the position of the matching closing brace considering nested braces."""
    count = 0
    for i, char in enumerate(text[start:], start=start):
        if (char == '{'):
            count += 1
        elif (char == '}'):
            count -= 1
            if (count == 0):
                return i
    return -1  # No matching closing brace found

def parse_cventry(text, start):
    """Parses a single \\cventry command with nested braces."""
    fields = []
    pos = start + len(r'\cventry')
    while (pos < len(text) and text[pos].isspace()):
        pos += 1  # Skip whitespace

    for _ in range(6):  # \cventry has 6 fields
        if (pos >= len(text) or text[pos] != '{'):
            break
        end = find_matching_brace(text, pos)
        if (end == -1):
            break  # No matching closing brace
        field_content = text[pos+1:end]  # Extract the content inside braces
        fields.append(field_content)
        pos = end + 1  # Move past the closing brace
        while (pos < len(text) and text[pos].isspace()):
            pos += 1  # Skip whitespace

    if (len(fields) == 6):
        return fields, pos
    else:
        return None, pos

def parse_latex_environment(content, env_name):
    """Parses LaTeX environments like itemize and description."""
    pattern = rf'\\begin\{{{env_name}\}}(.*?)\\end\{{{env_name}\}}'
    matches = re.finditer(pattern, content, re.DOTALL)
    for match in matches:
        env_content = match.group(1)
        if (env_name == 'itemize'):
            # Parse \item commands
            items = re.findall(r'\\item\s+(.*?)(?=\\item|\\end\{itemize\}|\Z)', env_content, re.DOTALL)
            filtered_items = [i for i in items if i]  # Remove empty strings
            html_list = ', '.join(item.strip() for item in filtered_items)
        elif (env_name == 'description'):
            # Parse \item[...] commands
            items = re.findall(r'\\item\[(.*?)\]\s*(.*?)(?=\\item|\Z)', env_content, re.DOTALL)
            html_list = '<br>' + '<br>'.join(f"<strong>{label.strip()}</strong>: {desc.strip()}" for label, desc in items)
        else:
            continue  # Unsupported environment

        # Replace the LaTeX environment with the HTML list
        content = content.replace(match.group(0), html_list)
    return content

def clean_field(field):
    """Enhanced field cleaning"""
    field = field.replace('``', '"').replace("''", '"').replace("`", "'").replace("'", "'")
    return field.strip()

def extract_personal_info(latex_content):
    """Extract personal information from LaTeX content"""
    info = {}
    info['first_name'] = re.search(r'\\name\{(.*?)\}\{', latex_content).group(1)
    info['last_name'] = re.search(r'\\name\{.*?\}\{(.*?)\}', latex_content).group(1)
    info['location'] = re.search(r'\\address\{(.*?)\}', latex_content).group(1)
    info['orcid'] = re.search(r'\\social\[orcid\]\{(.*?)\}', latex_content).group(1)
    return info

def parse_bibtex_file(filename):
    """Parse the BibTeX file and return entries"""
    with open(filename, 'r', encoding='utf-8') as bibtex_file:
        bib_database = bibtexparser.load(bibtex_file)
    print(f"Loaded {len(bib_database.entries)} BibTeX entries.")
    #print(bib_database.entries[0])
    return bib_database.entries

# HTML Generation Functions
def generate_header_html(info):
    """Generate HTML header from personal info"""
    return HEADER_TEMPLATE.format(
        first_name=info.get('first_name', ''),
        last_name=info.get('last_name', ''),
        location=info.get('location', ''),
        orcid=info.get('orcid', '')
    )

def generate_bio_html(bio_content):
    """Generate HTML for biography section"""
    return BIO_TEMPLATE.format(bio_content=bio_content)

def parse_authors(author_field):
    """Parse BibTeX author field into formatted string"""
    authors = author_field.split(' and ')
    formatted_authors = []
    for author in authors:
        parts = author.split(', ')
        if len(parts) == 2:
            lastname, firstname = parts
            initial = firstname[0] if firstname else ''
            formatted_authors.append(f"{lastname}, {initial}")
    return '; '.join(formatted_authors)

def format_doi_or_url(entry):
    """Format DOI or URL from a BibTeX entry"""
    doi = entry.get('doi', '')
    url = entry.get('url', '')

    if doi:
        return f'<br><a href="https://doi.org/{doi}" target="_blank" rel="noopener noreferrer">DOI: {doi}</a>.'
    elif url:
        #stripped_url = url.replace('http://', '').replace('https://', '')
        return f'<br><a href="{url}" target="_blank" rel="noopener noreferrer">URL: {url}</a>.'
    else:
        return ''

def format_journal_apa(entry):
    """Format journal, volume, and issue in APA style from a BibTeX entry"""
    journal = entry.get('journal', '')
    volume = entry.get('volume', '')
    issue = entry.get('number', '')
    pages = entry.get('pages', '')

    formatted_journal = f"<em>{journal}</em>"
    if volume:
        formatted_journal += f", <em>{volume}</em>"
    if issue:
        formatted_journal += f" ({issue})"
    if pages:
        formatted_journal += f", {pages}"

    return formatted_journal

def sort_entries_by_year(entries):
    """Sort entries by year with 'Under review' first, 'Forthcoming' second, and numeric years in descending order"""
    def sort_key(entry):
        year = entry[0].get('year', '')
        if year == 'Under review':
            return (0, '')
        elif year == 'Accepted':
            return (1, '')
        elif year.isdigit():
            return (2, -int(year))
        else:
            return (3, year)
    
    return sorted(entries, key=sort_key)

def format_book_or_incollection(entry):
    """Format book or incollection entry in APA style"""
    publisher = entry.get('publisher', '')
    address = entry.get('address', '')

    if entry['ENTRYTYPE'] == 'incollection':
        editors = parse_authors(entry.get('editor', ''))
        booktitle = entry.get('booktitle', '')
        pages = entry.get('pages', '')
        if pages:
            formatted_entry = f"In {editors} (Eds.), <em>{booktitle}</em> (pp. {pages})."
        else:
            formatted_entry = f"In {editors} (Eds.), <em>{booktitle}</em>."
        if address:
            formatted_entry += f" {address}: {publisher}"
        else:
            formatted_entry += f" {publisher}"
    else:
        if address:
            formatted_entry = f" {address}: {publisher}"
        else:
            formatted_entry = f" {publisher}"

    return formatted_entry

def generate_publications_html(entries):
    """Generate HTML for Publications section"""
    html_entries = ""
    # Group entries into Publication subcategories by ENTRYTYPE
    pub_sections = {
        "Journal articles": [],
        "Books and Book Chapters": [],
        "Other Publications": []
    }
    for entry in entries:
        if entry['ENTRYTYPE'] == 'article':
            entry_details = clean_latex_content(f"<strong>{entry.get('title', '')}</strong><br>{parse_authors(entry.get('author', ''))}. {format_journal_apa(entry)}. {format_doi_or_url(entry)}", LATEX_CHARS).replace('{', '').replace('}', '')
            pub_sections["Journal articles"].append((entry, entry_details))
        elif entry['ENTRYTYPE'] in ['book', 'incollection']:
            entry_details = clean_latex_content(f"<strong>{entry.get('title', '')}</strong><br>{parse_authors(entry.get('author', ''))}. {format_book_or_incollection(entry)}. {format_doi_or_url(entry)}", LATEX_CHARS).replace('{', '').replace('}', '')
            pub_sections["Books and Book Chapters"].append((entry, entry_details))
        else:
            entry_details = clean_latex_content(f"<strong>{entry.get('title', '')}</strong><br>{parse_authors(entry.get('author', ''))}. {format_doi_or_url(entry)}", LATEX_CHARS).replace('{', '').replace('}', '')
            pub_sections["Other Publications"].append((entry, entry_details))

    print(f"Grouped {len(entries)} BibTeX entries into {len(pub_sections)} subcategories.")
    # Generate HTML for each Publication subcategory
    for pub_section, items in pub_sections.items():
        print(f"Processing {len(items)} entries for '{pub_section}'")
        sorted_items = sort_entries_by_year(items)
        html_entries += f"<h3>{pub_section}</h3><table>"
        last_year = None
        for item in sorted_items:
            year = item[0].get('year', '')
            display_year = year if year != last_year else ''
            last_year = year
            html_entries += ENTRY_TEMPLATE.format(year=display_year, details=item[1])
        html_entries += "</table>"

    html_output = SECTION_TEMPLATE.format(section_id='publications', section_name='Publications', entries=html_entries)
    return html_output

# Section Processing Functions
def process_section(section_content, section_name, headers):
    """
    Processes a LaTeX section, extracting entries and converting them to HTML.
    """
    entries = []
    pos = 0
    while True:
        cventry_pos = section_content.find(r'\cventry', pos)
        if cventry_pos == -1:
            break

        entry, new_pos = parse_cventry(section_content, cventry_pos)
        if entry:
            entries.append(entry)
            pos = new_pos
        else:
            pos = cventry_pos + len(r'\cventry')

    if not entries:
        return "", 0

    # Generate HTML for the section
    html_entries = ""
    for entry in entries:
        # Unpack fields, separating year from other details
        year, *details = entry

        # Process LaTeX environments in details
        details = [parse_latex_environment(d, 'itemize') for d in details]
        details = [parse_latex_environment(d, 'description') for d in details]

        # Clean and process fields
        year = clean_field(year)
        details = [clean_field(d) for d in details]

        # Bold the position/title
        if details:
            filtered_details = [d for d in details[1:] if d]  # Remove empty strings
            details_str = f"<strong>{details[0]}</strong><br>" + ', '.join(filtered_details) if filtered_details else f"<strong>{details[0]}</strong>"

        # Add table row with line break between year and details
        html_entries += ENTRY_TEMPLATE.format(year=year, details=details_str)

    html_output = SECTION_TEMPLATE.format(section_id=section_name.lower().replace(' ', '-'), section_name=section_name, entries=html_entries)
    return html_output, len(entries)

def parse_section(latex_content, section_name, headers):
    """
    Extracts a section from LaTeX content and processes it.
    """
    pattern = rf'\\section\{{{section_name}\}}(.*?)(?=\\section|\Z)'
    match = re.search(pattern, latex_content, re.DOTALL)
    if not match:
        print(f"Section '{section_name}' not found.")
        return "", 0
    section_content = match.group(1).strip()
    return process_section(section_content, section_name, headers)

def main():
    if len(sys.argv) < 2:
        print("Usage: python latex_to_html.py <filename>")
        sys.exit(1)

    filename = sys.argv[1]

    with open(filename, 'r', encoding='utf-8') as file:
        latex_content = file.read()
    latex_content = clean_latex_content(latex_content, LATEX_CHARS)

    personal_info = extract_personal_info(latex_content)
    header_html = generate_header_html(personal_info)

    sections = SECTIONS
    toc = ""
    for section_name in sections.keys():
        toc += f'<li><a href="#{section_name.lower().replace(" ", "-")}">{section_name}</a></li>'

    bio_html = generate_bio_html(BIO)

    content = ""
    for section_name, headers in sections.items():
        if section_name == "Publications":
            bibtex_entries = parse_bibtex_file('../tex/pubs_ab.bib')
            publications_html = generate_publications_html(bibtex_entries)
            #publications_html = "Test"
            content += f'<div id="{section_name.lower().replace(" ", "-")}">{publications_html}</div>'
        else:
            section_html, entry_count = parse_section(latex_content, section_name, headers)
            if section_html:
                content += section_html
                print(f"Processed section '{section_name}' with {entry_count} entries.")

    html_output = HTML_TEMPLATE.format(header_html=header_html, toc=toc, bio_html=bio_html, content=content)

    with open('index.html', 'w', encoding='utf-8') as file:
        file.write(html_output)

if __name__ == "__main__":
    main()