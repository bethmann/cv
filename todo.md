# Curriculum Vitae Generator

## Overview
This project generates a curriculum vitae (CV) in both LaTeX and HTML formats. It includes various sections such as career, education, funding, research projects, presentations, teaching, research visits, consulting, commissions, and reviewer roles.

## Project Structure
- **.gitignore**: Specifies files and directories to be ignored by Git.
- **.gitlab-ci.yml**: CI/CD pipeline configuration for building and deploying the CV.
- **archive/**: Directory for archived files.
- **html/**: Directory containing HTML-related files.
  - **index.html**: Main HTML file for the CV.
  - **latex_to_html.py**: Script to convert LaTeX content to HTML.
  - **script.js**: JavaScript for dynamic behavior in the HTML CV.
  - **styles.css**: CSS for styling the HTML CV.
- **public/**: Directory for public-facing files.
  - **index.html**: Placeholder HTML file.
- **tex/**: Directory containing LaTeX-related files.
  - **cv.tex**: Main LaTeX file for the CV.
  - **pubs_ab.bib**: BibTeX file containing publication references.
- **README.md**: Project description and short bio.
- **todo.md**: To-do list for the project.

## Key Features
- **LaTeX to HTML Conversion**: Converts LaTeX content to HTML using `latex_to_html.py`.
- **CI/CD Pipeline**: Automated build and deployment using GitLab CI/CD.
- **Dynamic HTML CV**: JavaScript for updating active sections on scroll.
- **Responsive Design**: CSS for responsive layout and dark mode support.
- **Publications Parsing**: Parses a BibTeX file (`pubs_ab.bib`) to include publication references in the CV.

## Usage
1. **Building the CV**:
   - Run the LaTeX build process to generate the PDF.
   - Use the Python script to convert LaTeX to HTML.

2. **Deployment**:
   - The CI/CD pipeline handles the deployment of the generated CV.

## ToDos
- [ ] Add Publications Section in HTML
    - [ ] Parse BibTeX File
    - [ ] Include Publication References in the the same table style as for the other sections
    - [ ] Use APA Citation Style
    - [ ] Make subsections based on structure in tex/cv.tex
        - Journal articles: type=article; display Under Review entries first based on "Under review" keyword in bib file
        - Books or book chapters: type=inbook or type=book
        - Other publications: type=report or type=misc
        - Within each type, sort by year in descending order
- [ ] Implement Dark Mode Toggle
- [ ] Refactor Python Script for Improved Maintainability
- [ ] Refactor HTML and CSS for Improved Readability


